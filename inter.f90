﻿!****************************************************************************
!
!  PROGRAM: VP_02
!
!  PURPOSE: Модуль, производящий интерполяцию
!
!****************************************************************************
    
module inter
implicit none

    contains
    
    subroutine UniformInterpolation(a,b,n,Xk,Yk,X,Y)
    implicit none
    integer(4) :: i, n
    real(8) :: a, b, h, scale
    real(8), dimension(0:n) :: X, Y
    real(8), dimension(0:100*n) :: Xk, Yk
    
    h=(b-a)/(100*n)
    scale=(b-a)/n
    do i=0,n
        X(i)=a+i*scale
    enddo 
    
    call Lagrange(X,Y,Xk,Yk,h,n)
    end subroutine UniformInterpolation
    
    subroutine CebyshevInterpolation(a,b,n,Xk,Yk,X,Y)
    implicit none
    integer(4) :: i, n
    real(8) :: a, b, h, const, scale
    real(8), dimension(0:n) :: X, Y
    real(8), dimension(0:100*n) :: Xk, Yk
    real(8), parameter :: pi  = 4 * atan (1d0)
    
    const=pi/(2*n+2) !чтобы не вычслять эту константу в цикле много раз
    scale=a-(a+b)/2
    h=(b-a)/(100*n)
    do i=0,n
        X(i)=(a+b)/2+scale*cos((2*i+1)*const)
    enddo 
    
    call Lagrange(X,Y,Xk,Yk,h,n)
    end subroutine CebyshevInterpolation
    
    subroutine Lagrange(X,Y,Xk,Yk,h,n)
    implicit none
    integer(4) :: i, j, n, k
    real(8) :: h
    real(8), dimension(0:n) :: X, Y, fi, c, lagran
    real(8), dimension(0:100*n) :: Xk, Yk
    
    do i=0,n
        fi(i)=Y(i)/product(X(i)-X,(X(i)-X)/=0)
    enddo    
    do i=0,100*n
        Xk(i)=X(0)+h*i
    enddo
    do i=0,100*n
        do j=0,n
            C(j)=product(Xk(i)-X(0:j-1))*product(Xk(i)-X(j+1:n))
        enddo
        Lagran=fi*c
        Yk(i)=sum(Lagran)
    enddo    
    
    end subroutine Lagrange
end module inter